# Ansible Collection - consensus.wireguard_cloud_gateway

Contains the `consensus.wireguard_cloud_gateway` role.  See [the role's README](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-wireguard-cloud-gateway/-/blob/master/roles/wireguard_cloud_gateway/README.md) for more information.
